import React, { Component } from 'react';
import Form from './components/Form';
import List from './components/List';

class App extends Component {
  state = {
    data:[]
  }

  componentDidMount(){
    const dataStorage = JSON.parse(localStorage.getItem('dataStorage'));
    if(dataStorage !== null){
      this.setState({data: dataStorage})
    }
  }

  componentDidUpdate() {
    localStorage.setItem('dataStorage', JSON.stringify(this.state.data))
  }

  handleSubmit = (newTodo) => {
    this.setState({
      data: [...this.state.data, newTodo]
    })
  };

  handleRemove = index => {
    const {data} = this.state;
    this.setState({
      data: data.filter((item, i) => {
        return i !== index
      })
    })
  };

  handleOnEdit = (editVal, index) => {
    const {data} = this.state;
    data.forEach((item, i) => {
      if(i === index){
        item.todo = editVal
      }
    });
    this.setState({data})
  }

  render() {
    const { data } = this.state;

    return(
      <>
        <div className='bg-image'>
          
        </div>
        <div className='app'>
          <h1>ToDo List</h1>
          <Form onSubmit={this.handleSubmit} />
            {data.length === 0
            ?
            <h2>Nothing left to do</h2>
            : 
            <List todo={data} onDelete={this.handleRemove} onEdit={this.handleOnEdit} />
          }
        </div>
      </>
    );
  }
}

export default App;
